﻿using System;
using System.Windows.Forms;
using System.Text;
using System.Net.Sockets;
using System.Threading;

namespace netChet
{
    public partial class Form1 : Form
    {
        TcpClient clientSocket = new TcpClient();
        NetworkStream serverStream = default(NetworkStream);
        string readData = null;
        bool conect = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonMasge_Click(object sender, EventArgs e)
        {
            sendChat(textBoxMesge.Text);
        }

        private void buttonConect_Click(object sender, EventArgs e)
        {
            conect = true;
            readData = "Conected to Chat Server ...";
            msg();
            clientSocket.Connect(textBoxIp.Text, 8888);
            serverStream = clientSocket.GetStream();

            byte[] outStream = Encoding.ASCII.GetBytes(textBoxName.Text + "$");
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();

            Thread ctThread = new Thread(getMessage);
            ctThread.Start();
        }

        private void getMessage()
        {
            while (true)
            {
                serverStream = clientSocket.GetStream();
                byte[] inStream = new byte[byte.MaxValue];
                serverStream.Read(inStream, 0, byte.MaxValue);
                string returndata = Encoding.ASCII.GetString(inStream);
                
                readData = "" + returndata;
                msg();
            }
        }

        private void msg()
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(msg));
            else
                textBox2.Text = textBox2.Text + Environment.NewLine + readData;
        }
        public void sendChat(string massge)
        {
            byte[] outStream = System.Text.Encoding.ASCII.GetBytes(massge + "$");
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (conect)
                sendChat("exit");
        }
    }
    public class client
    {
        private TcpClient clientSocket = new TcpClient();
        private NetworkStream serverStream = default(NetworkStream);
        private bool conect = false;
        private bool isConect
        {
            get
            {
                return conect;
            }
        }
        
        public bool Conect()
        {
            return true;
        }

        public void Send(string massge)
        {
            byte[] outStream = Encoding.ASCII.GetBytes(massge + "$");
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();
        }
        public void GetMessageAll()
        {
            while (true)
            {
                serverStream = clientSocket.GetStream();
                byte[] inStream = new byte[byte.MaxValue];
                serverStream.Read(inStream, 0, byte.MaxValue);
                string returndata = Encoding.ASCII.GetString(inStream);

                if (getMessageByte != null)
                    getMessageByte(inStream);
                if (getMessage != null)
                    getMessage(returndata);
                
            }
        }
        public delegate void GetMessage(string Message);
        public delegate void GetMessageByte(Byte[] Message);
        public GetMessage getMessage = null;
        public GetMessageByte getMessageByte = null;
    }
}